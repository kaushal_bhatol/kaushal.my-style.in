<?php
include 'head.php';
?>
<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>ME | <?=$userName?></title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="icon" href="<?=$userPhoto?>" type="image/x-icon" />
</head>

<body>
    <div id="profile">
        <img id="userPhoto" src="<?=$userPhoto?>" alt="<?=$userPhotoAlt?>">
        <div id="userName">
            <h1><?=$userName;?></h1>
        </div>
    </div>
    <div id="links">
        <a class="link <?=$visible1?>" href="<?=$link1?>" target="_blank"><?=$link1Title?></a>
        <a class="link <?=$visible2?>" href="<?=$link2?>" target="_blank"><?=$link2Title?></a>
        <a class="link <?=$visible3?>" href="<?=$link3?>" target="_blank"><?=$link3Title?></a>
        <a class="link <?=$visible4?>" href="<?=$link4?>" target="_blank"><?=$link4Title?></a>
    </div>
    <div class="footer">
        <a href="<?=$github ?>" target="_blank">
            <i style="line-height: 38px;" class="fa fa-github circulo" aria-hidden="true"></i>
        </a>
        <a href="<?=$instagram?>" target="_blank">
            <i style="line-height: 38px;" class="fa fa-instagram circulo" aria-hidden="true"></i>
        </a>
        <a href="<?=$fb?>" target="_blank">
            <i style="line-height: 38px;" class="fa fa-facebook-f circulo" aria-hidden="true"></i>
        </a>
        <a href="<?=$twitter?>" target="_blank">
            <i style="line-height: 38px;" class="fa fa-twitter circulo" aria-hidden="true"></i>
        </a>
        <a href="mailto:<?=$email?>" target="_blank">
            <i style="line-height: 38px;" class="fa fa-envelope circulo" aria-hidden="true"></i>
        </a>
    </div>

</body>
</html>