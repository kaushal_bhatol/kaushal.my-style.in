<?php
// Profile
$userName = 'Kaushal Bhatol';
$userPhoto = 'assets/images/profile.png';
$userPhotoAlt = 'Profile picture of Kaushal Bhatol';

// links
// visible none value = hide
$visible1 = 'show';
$link1Title = 'kaushal.iblogger.org';
$link1 = 'http://kaushal.iblogger.org/?i=1';

$visible2 = 'show';
$link2Title = 'kaushal.unaux.com';
$link2 = 'http://kaushal.unaux.com/';

$visible3 = '';
$link3Title = '';
$link3 = '';

$visible4 = '';
$link4Title = '';
$link4 = '';

// footer links
$github = 'http://github.com/KaushalBhatol';
$instagram = 'https://instagram.com/kaushal_bhatol';
$fb = 'https://www.facebook.com/kaushal.bhatol38';
$twitter = 'https://twitter.com/kaushal_bhatol';
$email = 'kaushal.bhatol@outlook.com';

?>